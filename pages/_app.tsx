import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { ReactElement, ReactNode } from 'react'
import { NextPage } from 'next'
import { MicroStacksProvider } from 'micro-stacks/react'

type NextPageWithLayout = NextPage & {
  getLayout?: (page: ReactElement) => ReactNode
}

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout
}

const authOptions = {
  appDetails: {
    name: 'my cool app',
    icon: '/some-logo.png'
  }
}


const network = 'mainnet';

function MyApp({ Component, pageProps }: AppPropsWithLayout) {
  const getLayout = Component.getLayout ?? ((page) => page)
  return (
    <MicroStacksProvider authOptions={authOptions} network={network} >
        {getLayout(<Component {...pageProps} />)}
    </MicroStacksProvider>
  )
}
export default MyApp
