import React, { ReactElement, useState } from "react";
import Image from 'next/image';
import { WorldMap } from "../components/worldMap";
import { LightWorldMap } from "../components/lightWorldMap";
import ScrollDrag from "../components/scrollDrag";
import SvgMap from "../components/svgMap";
import Layout from "../components/layout";
import { Popover } from "@mui/material";
import { GetQueries } from 'jotai-query-toolkit/nextjs';
import { appProviderAtomBuilder } from 'micro-stacks/react';
import { StacksMainnet, StacksMocknet, StacksRegtest } from 'micro-stacks/network';

import {
    DEFAULT_MAINNET_SERVER,
    DEFAULT_REGTEST_SERVER,
    DEFAULT_LOCALNET_SERVER,
    ENV,
  } from '../utils/constants';

type Props = {
    isMobile: boolean;
};


export default function MapPage() {
    const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);
    const open = Boolean(anchorEl);
    const [pos, setPos] = useState({left: 0, top:0});

    const handleClick = (event: any) => {
        console.log(event);
        setPos({left: event.clientX, top: event.clientY})
        setAnchorEl(event.target);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
            <ScrollDrag rootClass={'drag-container'} >
                <SvgMap onClick={handleClick} />
                <Popover
                    open={open}
                    onClose={handleClose}
                    anchorPosition={pos}
                    anchorEl={anchorEl}
                >
                    Hey
                </Popover>
            </ScrollDrag>
    )
}

MapPage.getLayout = function getLayout(page: ReactElement) {
    return (
        <Layout>
            {page}
        </Layout>
    )
}

const initialNetwork =
  ENV === 'development'
    ? new StacksMocknet({ url: DEFAULT_LOCALNET_SERVER })
    : new StacksMainnet({ url: DEFAULT_MAINNET_SERVER });

