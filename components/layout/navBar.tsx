import { AppBar, Button, IconButton, SvgIcon, Toolbar, Typography } from '@mui/material';
import { useAuth } from 'micro-stacks/react';
import AccountBalanceWalletIcon from '@mui/icons-material/AccountBalanceWallet';
import { Box } from '@mui/system';
import React from 'react';
import { installWalletDialogAtom } from '../../store/connection';
import { useAtom } from 'jotai';

export default function Navbar() {
    const { isSignedIn, handleSignIn, handleSignOut, isLoading, session } = useAuth();
    const [open, setOpen] = useAtom(installWalletDialogAtom);
    
    return (
        <Box sx={{ flexGrow: 1}}>
            <AppBar color="transparent" position="sticky" >
                <Toolbar>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        World Map
                    </Typography>
                    <Button
                        variant="contained"
                        endIcon={<AccountBalanceWalletIcon />}
                        onClick={
                            isSignedIn
                            ? () => handleSignOut()
                            : () => {
                                try {
                                    handleSignIn();
                                } catch (_e) {
                                    console.log(_e);
                                }

                                !session && setOpen(true);
                            }
                        }
                    >
                        {isLoading ? `Loading...` : isSignedIn ? `Sign out` : `Connect STX Wallet`}
                    </Button>
                </Toolbar>
            </AppBar>
        </Box>
    )
}