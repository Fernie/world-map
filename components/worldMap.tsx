import React, { FunctionComponent, useEffect, useRef, useState } from "react";
import useDraggableScroll from "use-draggable-scroll";
import ScrollContainer from 'react-indiana-drag-scroll'
import Image from "next/image";
import WorldMapImage from "../public/map.png"
import SvgMapCompressed from "./compressSvgMap";
// import SvgMap from "./svgMap";

export const WorldMap: FunctionComponent<{}> = ({}) => {
    const ref = useRef(null);
    const input = useRef(null);
  
    const { onMouseDown } = useDraggableScroll(ref);

    const onClick = ((event: any) => {
        console.log(event.target)
        if (event.target.tagName === "INPUT") {
            input.current = event.target;
          event.target.focus();
        } else {
          if (input.current) {
            input.current = null;
          }
          event.target.click();
        }
      });

    const reactToThis = (e: any) => {
        console.log(e);
        if(ref.current) {
            console.log(ref.current.scrollTop, ref.current.scrollLeft)
        }
    }
    
    return (
        // <div
        //     style={{ position: "relative", width: 2560, height: 1301 }}
        //     onDragStart={(e) => dragStart(e)}
        //     onDragEnd={(e) => dragging(e)}
        // >
        // </div>
        <ScrollContainer className="scroll-container" hideScrollbars={true} stopPropagation={true}  onClick={onClick}>
            {/* <div style={{ position: "relative", width: 2560, height: 1301 }} onClick={reactToThis} >  */}
                <SvgMapCompressed />
            {/* </div> */}
        </ScrollContainer>
        // <Image layout="fixed" width="2560" height="1301" src={WorldMapImage} alt="Map" />

    )
}