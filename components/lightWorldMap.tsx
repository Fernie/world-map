import React, { FunctionComponent, useEffect, useRef, useState } from "react";
import useDraggableScroll from "use-draggable-scroll";
import ScrollContainer from 'react-indiana-drag-scroll'
import Image from "next/image";
import WorldMapImage from "../public/map.png"
import SvgMapCompressed from "./compressSvgMap";
import SvgMap from "./heavyWorldMap";
// import SvgMap from "./svgMap";


export const LightWorldMap: FunctionComponent<{}> = ({}) => {
    const [styles, setStyles] = useState({});
    const [diffPos, setDiffPos] = useState({ diffX: 0, diffY: 0 });
    const [isDragging, setIsDragging] = useState(false);
    const [pos, setPos] = useState({ x: 0, y: 0, scale: 1 });

  const dragStart = (e) => {
    const boundingRect = e.currentTarget.getBoundingClientRect();

    setDiffPos({
      diffX: e.clientX - boundingRect.left,
      diffY: e.clientY - boundingRect.top
    });

    setIsDragging(true);

    return true;
  };

  const dragging = (e) => {
      console.log(e);
    const left = e.clientX - diffPos.diffX;
    const top = e.clientY - diffPos.diffY;

    setStyles({ left: left, top: top });
  };

  const dragEnd = () => {
    setIsDragging(false);
  };
  
    const dragTo = (e) => {
        dragging(e);
        dragEnd();
    }

    const onScroll = (e) => {
        const delta = e.deltaY * -0.005;
        const newScale = pos.scale + delta;

        const ratio = 1 - newScale / pos.scale;

        setPos({
            scale: newScale,
            x: pos.x + (e.clientX - pos.x) * ratio,
            y: pos.y + (e.clientY - pos.y) * ratio
        });
    };


    return (
        <div
            onWheelCapture={onScroll}
            style={{
                ...styles,
                position: "relative",
                width: 2560,
                height: 1301,
                transformOrigin: "0 0",
                transform: `translate(${pos.x}px, ${pos.y}px) scale(${pos.scale})`
            }}
            // onDrag={(e) => dragging(e)}
            onDragStart={(e) => dragStart(e)}
            onDrag={(e) => console.log(e)}
            onDragEnd={(e) => dragTo(e)}
            draggable
        >
            <SvgMap
                className="svg-map"
                onClick={(e) => console.log(e)}
                
            />
        </div>
    )


    // return (
    //     <SvgMapCompressed />
    // )
}